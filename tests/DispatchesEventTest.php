<?php

namespace Symbiont\Dispatcher\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Dispatcher\Concerns\DispatchesEvent;
use Symbiont\Dispatcher\Contracts\Callbackable;
use Symbiont\Dispatcher\Contracts\Dispatchable;
use Symbiont\Dispatcher\Contracts\Dispatching;
use Symbiont\Dispatcher\Dispatcher;

final class DispatchesEventTest extends TestCase {

    protected object $object;

    protected function setUp(): void {
        $this->object = new class implements Callbackable {
            use DispatchesEvent;

            public function test() {
                $this->trigger('testing');
            }
        };
    }

    public function testOnEvent() {
        $called = false;

        $this->object->on('testing', function() use(&$called) {
            $called = true;
        });

        $this->object->test();
        $this->assertTrue($called);
    }

    public function testOnceEvent() {
        $called = 0;

        $this->object->once('testing', function() use(&$called) {
            $called++;
        });

        $this->object->test();
        $this->object->test();
        $this->assertTrue($called === 1);
    }

    public function testOff() {
        $called = 0;

        $this->object->on('testing', function() use(&$called) {
            $called++;
        });

        $this->object->test();
        $this->assertTrue($called === 1);

        $this->object->off('testing');
        $this->object->test();
        $this->assertTrue($called === 1);
    }

    public function testOffCallback() {
        $called0 = 0;
        $called1 = 0;

        $callback0 = function() use(&$called0) {
            $called0++;
        };
        $callback1 = function() use(&$called1) {
            $called1++;
        };

        $this->object->on('testing', $callback0);
        $this->object->on('testing', $callback1);

        $this->object->test();
        $this->assertTrue($called0 === 1);
        $this->assertTrue($called1 === 1);

        $this->object->off('testing', $callback0);
        $this->object->test();
        $this->assertTrue($called0 === 1);
        $this->assertTrue($called1 === 2);
        $this->object->test();
        $this->assertTrue($called0 === 1);
        $this->assertTrue($called1 === 3);
    }

    public function testOffAll() {
        $this->object = new class implements Callbackable {
            use DispatchesEvent {
               DispatchesEvent::makeDispatcherInstance as makeDispatcherInstanceTrait;
            }

            protected function makeDispatcherInstance(): Dispatching {
                return new class extends Dispatcher {
                    public function getEvents(): array {
                        return $this->events;
                    }
                };
            }

            public function getDispatcherEvents() {
                return $this->dispatcher->getEvents();
            }

            public function test() {
                $this->trigger('testing');
            }
        };

        $this->object->on('testing', function() {});
        $this->object->on('testing', function() {});
        $this->object->on('tester', function() {});

        $events = $this->object->getDispatcherEvents();
        $this->assertTrue(count($events) === 2);
        $this->assertTrue(count($events['testing']) === 2);
        $this->assertTrue(count($events['tester']) === 1);

        $this->object->offAll();
        $events = $this->object->getDispatcherEvents();
        $this->assertTrue(count($events) === 0);
        $this->assertFalse(array_key_exists('testing', $events));
        $this->assertFalse(array_key_exists('tester', $events));
    }
}