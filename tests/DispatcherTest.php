<?php

namespace Symbiont\Dispatcher\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Dispatcher\Contracts\Dispatching;
use Symbiont\Dispatcher\Dispatcher;

final class DispatcherTest extends TestCase {

    protected ?Dispatching $dispatcher;

    protected function setUp(): void {
        $this->dispatcher = new class extends Dispatcher {
            public function getEvents(): array {
                return $this->events;
            }
        };
    }

    public function testRegisterEvent() {
        $callback0 = function() { return 'callback at index 0'; };
        $callback1 = function() { return 'callback at index 1'; };

        $this->dispatcher->register('testing', $callback0);
        $events = $this->dispatcher->getEvents();

        $this->assertTrue(count($events) === 1);
        $this->assertEquals($callback0, $events['testing'][0]);

        $this->dispatcher->register('testing', $callback1);
        $events = $this->dispatcher->getEvents();

        $this->assertTrue(count($events) === 1);
        $this->assertTrue(count($events['testing']) === 2);
        $this->assertEquals($callback0, $events['testing'][0]);
        $this->assertEquals($callback1, $events['testing'][1]);
    }

    public function testDispatchEvent() {
        $called = false;
        $this->dispatcher->register('testing', function() use(&$called) {
            $called = true;
        });

        $this->assertFalse($called);
        $this->dispatcher->dispatch('testing');
        $this->assertTrue($called);

        $called = false;
        $this->dispatcher->dispatch('tester');
        $this->assertFalse($called);
    }

    public function testOnceEvent() {
        $called = 0;

        $this->dispatcher->once('testing', function() use(&$called) {
            $called++;
        });

        $this->assertTrue(count($this->dispatcher->getEvents()) === 1);
        $this->assertTrue(count($this->dispatcher->getEvents()['testing']) === 1);
        $this->assertEquals(0, $called);
        $this->dispatcher->dispatch('testing');
        $this->assertEquals(1, $called);

        $this->assertTrue(count($this->dispatcher->getEvents()) === 1);
        $this->assertTrue(count($this->dispatcher->getEvents()['testing']) === 0);
        $this->dispatcher->dispatch('testing');
        $this->assertEquals(1, $called);
    }

    public function testReset() {
        $this->dispatcher->register('testing', function() {});
        $this->dispatcher->register('tester', function() {});

        $this->assertTrue(count($this->dispatcher->getEvents()) === 2);
        $this->dispatcher->reset();
        $this->assertTrue(count($this->dispatcher->getEvents()) === 0);
    }

    public function testUnregister() {
        $this->dispatcher->register('testing', function() {});
        $this->dispatcher->register('tester', function() {});
        $this->assertTrue(count($this->dispatcher->getEvents()) === 2);

        $this->dispatcher->unregister('testing');
        $this->assertTrue(count($this->dispatcher->getEvents()) === 1);

        $this->dispatcher->unregister('tester');
        $this->assertTrue(count($this->dispatcher->getEvents()) === 0);
    }

    public function testUnregisterEvent() {
        $callback0 = function() { return 'callback at index 0'; };
        $callback1 = function() { return 'callback at index 1'; };

        $this->dispatcher->register('testing', $callback0);
        $this->dispatcher->register('testing', $callback1);
        $this->assertTrue(count($this->dispatcher->getEvents()) === 1);
        $this->assertTrue(count($this->dispatcher->getEvents()['testing']) === 2);

        $this->dispatcher->unregister('testing', $callback0);
        $this->assertTrue(count($this->dispatcher->getEvents()) === 1);
        $this->assertTrue(count($this->dispatcher->getEvents()['testing']) === 1);
        $this->assertEquals($callback1, $this->dispatcher->getEvents()['testing'][0]);

        $this->dispatcher->unregister('testing', $callback1);
        $this->assertTrue(array_key_exists('testing', $this->dispatcher->getEvents()));
        $this->assertTrue(count($this->dispatcher->getEvents()['testing']) === 0);
    }

    public function testRegisteredEvent() {
        $callback = function() {
            return 'some test callback';
        };
        $callback_not_registered = function() {

        };
        $this->dispatcher->register('test', $callback);
        $this->dispatcher->register('test', function() {});

        $this->assertTrue($this->dispatcher->registered('test'));
        $this->assertFalse($this->dispatcher->registered('tester'));

        $this->assertTrue($this->dispatcher->registered('test', $callback));
        $this->assertFalse($this->dispatcher->registered('test', $callback_not_registered));
        $this->assertFalse($this->dispatcher->registered('tester', $callback));
    }
}