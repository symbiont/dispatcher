<?php

namespace Symbiont\Dispatcher;

use Symbiont\Dispatcher\Contracts\Dispatchable;
use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;
use Symbiont\Support\ForwardCall\ForwardsCall;

/**
 * @method register
 * @method unregister
 * @method once
 * @method dispatch
 * @method reset
 */
final class EventDispatcher implements ForwardsCalls {

    use ForwardsCall;

    protected static ?Dispatchable $dispatcher = null;

    protected static function initialize() {
        if(! static::$dispatcher) {
            static::$dispatcher = new Dispatcher;
        }
    }

    public static function forwardDispatcher() {
        static::initialize();

        return ['register', 'unregister', 'registered', 'once', 'dispatch', 'reset'];
    }

    public static function getDispatcher(): Dispatchable {
        static::initialized();

        return self::$dispatcher;
    }

}