<?php

namespace Symbiont\Dispatcher\Contracts;

use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;

/**
 * Callbackable interface for any contract made with a class using DispatchesEvents
 */
interface Callbackable extends ForwardsCalls, Dispatchable {  }