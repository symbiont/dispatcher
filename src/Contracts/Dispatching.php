<?php

namespace Symbiont\Dispatcher\Contracts;

/**
 * Dispatcher interface
 */
interface Dispatching extends Dispatchable {

    public function register(string|array $name, callable $callback): Dispatchable;
    public function registered(string $name, ?callable $callback = null): bool;
    public function once(string|array $name, callable $callback): Dispatchable;
    public function unregister(string|array $names, ?callable $callback = null): Dispatchable;
    public function dispatch(string $name, mixed $args): Dispatchable;
    public function reset(): Dispatchable;

}