<?php

namespace Symbiont\Dispatcher\Contracts;

/**
 * Dispatchable interface for any contract made with a class using HandlesDispatcher
 */
interface Dispatchable {  }