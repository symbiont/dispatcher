<?php

namespace Symbiont\Dispatcher;

use Symbiont\Dispatcher\Contracts\{Dispatching, Dispatchable};

class Dispatcher implements Dispatching {

    protected array $events = [];

    public function register(string|array $name, callable $callback): Dispatchable {
        $names = is_string($name) ? [$name] : $name;

        foreach($names as $name) {
            $this->events[$name][] = $callback;
        }

        return $this;
    }

    public function registered(string $name, ?callable $callback = null): bool {
        if($callback !== null) {
            if(! array_key_exists($name, $this->events)) {
                return false;
            }

            return array_search($callback, $this->events[$name]) !== false;
        }

        return array_key_exists($name, $this->events);
    }

    public function once(string|array $name, callable $callback): Dispatchable {
        $unregistering_callback = function() use ($name, $callback, &$unregistering_callback) {
            call_user_func_array($callback, func_get_args());
            $this->unregister($name, $unregistering_callback);
        };
        return $this->register($name, $unregistering_callback);
    }

    public function dispatch(string $name, mixed $args = []): Dispatchable {
        if (array_key_exists($name, $this->events)) {
            if(!is_array($args)) {
                $args = [$args];
            }
            array_unshift($args, $name);

            foreach($this->events[$name] as $event) {
                call_user_func_array($event, $args);
            }

        }

        return $this;
    }

    public function unregister(string|array $names, ?callable $callback = null): Dispatchable {
        $names = is_string($names) ? [$names] : $names;

        foreach($names as $name) {
            if(is_array($name)) {
                throw new Exceptions\Exception('Unregistering nested arrays not supported!');
            }

            if(! array_key_exists($name, $this->events)) {
                continue;
            }

            if(! $callback) {
                unset($this->events[$name]);
                continue;
            }

            foreach($this->events[$name] as $callback_index => $callback_event) {
                if ($callback_event === $callback) {
                    unset($this->events[$name][$callback_index]);
                }
            }

            // re-index
            $this->events[$name] = array_values($this->events[$name]);
        }

        return $this;
    }

    /**
     * Reset all events
     *
     * @return void
     */
    public function reset(): Dispatchable {
        $this->events = [];

        return $this;
    }

}