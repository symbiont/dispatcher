<?php

namespace Symbiont\Dispatcher\Concerns;

use Symbiont\Support\ForwardCall\ForwardsCall;

use Symbiont\Dispatcher\Contracts\Dispatching;
use Symbiont\Dispatcher\Dispatcher;

trait DispatchesEvent {

    use ForwardsCall;

    protected ?object $dispatcher = null;

    protected function initializeDispatcher() {
        if(! $this->dispatcher) {
            $this->dispatcher = $this->makeDispatcherInstance();
        }
    }

    protected function makeDispatcherInstance(): Dispatching {
        return new Dispatcher;
    }

    public function forwardDispatcher(): array {
        $this->initializeDispatcher();

        return [
            'on' => 'register',
            'off' => 'unregister',
            'offAll' => 'reset',
            'trigger' => 'dispatch',
            'once'
        ];
    }

}