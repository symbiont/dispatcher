<?php

namespace Symbiont\Dispatcher\Exceptions;

class EventNameTypeException extends Exception  {
    public function __construct(string $name) {
        parent::__construct(
            sprintf('Only string type as event name allowed, given %s', $name)
        );
    }
}