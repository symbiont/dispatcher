<?php

namespace Symbiont\Dispatcher\Exceptions;

class InvalidArgumentException extends Exception  {}