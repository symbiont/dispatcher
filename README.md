# Symbiont / Dispatcher

Simple event dispatcher

This package is work in progress!

## Requirements

- `php 8.2`

## Installation

```bash
composer require symbiont/dispatcher
```

## Usage

Using `EventDispatcher` and `Dispatcher`.

```php
use Symbiont\Dispatcher\{Dispatcher, EventDispatcher};

// global event dispatcher
EventDispatcher::register('test', function() {
    return 'test';
});
EventDispatcher::once('test', function() {
    return 'once';
})
EventDispatcher::dispatch('test');

// local event dispatcher
$dispatcher = new Dispatcher;
$dispatcher->register('test', function() {
    return 'test';
});
$dispatcher->once('test', function() {
    return 'once';
});
$dispatcher->dispatch('test');
```

Implementing an event dispatcher in terms of callbacks using `Callbackable` and `DispatchesEvent` trait.

```php
use Symbiont\Dispatcher\Contracts\Callbackable;
use Symbiont\Dispatcher\Concerns\DispatchesEvent;

class Awesome implements Callbackable {
    use DispatchesEvent;
    
    public function load() {
        $this->trigger('beforeLoad');
        // something to load ..
        $this->trigger('afterLoad');
    }
}

$awesome = new Awesome;
$awesome->on('afterLoad', function() {
    // something to do after load ..
});
$awesome->load();
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: https://symbiont.gitlab.io/dispatcher
- Gitlab: [https://gitlab.com/symbiont/dispatcher](https://gitlab.com/symbiont/dispatcher)

## Tests

```bash
composer test
```

---

## License

[MIT license](LICENSE.MD)