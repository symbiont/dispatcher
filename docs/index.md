# Dispatcher

Simple event dispatcher.

!!! Note
    This package is `work in progress`!

## Requirements

 - PHP `8.2`

```bash
composer require symbiont/dispatcher
```

## Usage

```php
<?php

use Symbiont\Dispatcher\{EventDispatcher, Dispatcher};

// global
EventDispatcher::register('event', function($argument) { /* callback */ });
EventDispatcher::dispatch('event', ['some-argument']);
EventDispatcher::once('once-only', function($argument) { /* callback */ });
EventDispatcher::dispatch('once-only');

// object
$dispatcher = new Dispatcher;
$dispatcher
    ->register('event', function($argument) { /* callback */ })
    ->dispatch('event', ['some-argument'])
    ->once('once-only', function() { /* callback called once */ })
    ->dispatch('once-only');
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/dispatcher](https://symbiont.gitlab.io/dispatcher)
- Gitlab: [https://gitlab.com/symbiont/dispatcher](https://gitlab.com/symbiont/dispatcher)

---

## Testing

```php
composer test
```

## License

Released under MIT License