# Usage

Simple global/local dispatcher library.

## Classes (Global/Local)

 - Class `EventDispatcher` for global use
 - Class `Dispatcher` for local use (object)

```php
<?php
EventDispatcher::register('event', function() {});
$dispatcher = new Dispatcher;
$dispatcher->register('event', function() {});
```

## Methods

With `registered` the only method returning a `boolean`, all other methods can be chained.

```php
<?php
(new Dispatcher)
    ->register('event', function() {})
    ->once('once-only', function() {})
    ->dispatch('another')
    ->reset();
```

| Name                                             |                                                  |      |
|:-------------------------------------------------|:-------------------------------------------------|------|
| `register(string $event, callable $callback)`    | Register event with given callback               | self |
| `once(string $event, callable $callback)`        | Register event only once with given callback     | self |
| `registered(string $event)`                      | If event is registered                           | bool |
| `unregister(string $event, ?callable $callback)` | Unregister event with optional specific callback | self |
| `dispatch(string\|array $event)`                 | Dispatch event(s)                                | self |
| `reset()`                                        | Remove all registered events                     | self |

### Registering events

To register events, `register` and `once` are used, where `once` deletes the callback after it has been called.

```php
<?php
(new Dispatcher)
    ->register('event', function() { /* called always */ });
    ->once('event', function() { /* called once */ })
```

### Registered

```php
<?php
(new Dispatcher)
    ->register('event', function() {})
    ->registered('event') // returns `true`

```

### Unregister

Unregister all callbacks for given event if no callback is applied. If a callback is applied, it only removes 
the callback from the event list.

```php
<?php
$callback = function() {
    /* some callback */
};
(new Dispatcher)
    ->register('some-event', $callback)
    ->unregister('some-event') // removes all callbacks from `some-event`
    ->unregister('some-event', $callback) // removes only `$callback` from the event list `some-event`
```

### Dispatch

```php
// globally
EventDispatcher::dispatch('some-event');
// locally
(new Dispatcher)->dispatch('some-event');
```

### Reset

Remove all registered events

```php
<?php
// globally
EventDispatcher::reset();
// locally
(new Dispatcher)->reset();
```

## Callback trait for classes

Any class can implement the `Callbackable` interface with the `DispatchesEvents` trait. Doing so, 5 methods will
be forwarded to the dispatcher object.

| Name                                           | Forwarded    |
|:-----------------------------------------------|--------------|
| on(array\|string $event, callable $callback)   | `register`   |
| once(array\|string $event, callable $callback) | `once`       |
| off(array\|string $event, ?callable $callback) | `unregister` |
| offAll()                                       | `reset`      |
| trigger()                                      | `dispatch`   |

```php
<?php
use Symbiont\Dispatcher\Contracts\Callbackable;
use Symbiont\Dispatcher\Concerns\DispatchesEvent;

class SomeClass implements Callbackable {
    use DispatchesEvent;
}

$some = new SomeClass;
$some
    ->on('something', function() {})
    ->once('once-only', function() {})
    ->trigger('something');
```

To customize the dispatcher object created by the `DispatchesEvent` trait, overwrite the `makeDispatcherInstance` method.

```php
<?php
use Symbiont\Dispatcher\Contracts\Callbackable;
use Symbiont\Dispatcher\Concerns\DispatchesEvent;

class SomeClass implements Callbackable {
    use DispatchesEvent {
        DispatchesEvent::makeDispatcherInstance as private makeDispatcherInstanceOriginal
    };
    
    protected function makeDispatcherInstance() : Dispatching {
        $dispatcher = new Dispatcher;
        // anything you that needs be to change
        return $dispatcher;
    }
}
```

The `DispatchesEvent` has different method names in order to differentiate between a `Dispatcher` object and a class 
implementing the `DispatchesEvent` trait.

